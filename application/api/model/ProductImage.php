<?php

namespace app\api\model;

use think\Db;

class ProductImage extends BaseModel
{
    protected $hidden = ['delete_time', 'create_time', 'update_time'];

    public function imgUrl()
    {
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}