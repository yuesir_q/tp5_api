<?php

namespace app\api\model;

use think\Db;

class Theme extends BaseModel
{
    protected $hidden = ['delete_time', 'update_time'];
    //首页展示图片
    public function topicImg()
    {
//        return $this->hasOne('Image', 'id', 'topic_img_id');
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

    //列表展示图片
    public function headImg()
    {
        return $this->belongsTo('Image', 'head_img_id', 'id');
//        return $this->hasOne('Image', 'id', 'head_img_id');
    }
    
    public function products()
    {
        return $this->belongsToMany('Product', 'theme_product', 'product_id', 'theme_id');
    }

    /**
     * @route api/v1/theme/:id
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getThemeWithProducts($id)
    {
        $theme = self::with('topicImg,headImg,products')->find($id);

        return $theme;
    }
}