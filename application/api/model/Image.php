<?php

namespace app\api\model;


class Image extends BaseModel
{
    protected $hidden = ['id', 'from', 'delete_time', 'update_time'];

    //修正图片路径, 将相对地址改为绝对地址
    public function getUrlAttr($value, $data)
    {
        return parent::getUrl($value, $data);
    }
}
