<?php

namespace app\api\model;

use think\Model;

class BaseModel extends Model
{
    //获取地址, 将相对地址转换为绝对地址
    protected function getUrl($value, $data)
    {
        $finalUrl = $value;
        if ( 1 == $data['from']) {
            $finalUrl = config('setting.img_prefix').$value;
        }

        return $finalUrl;
    }
}
