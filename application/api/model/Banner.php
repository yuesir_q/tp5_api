<?php

namespace app\api\model;

use think\Db;

class Banner extends BaseModel
{
    protected $hidden = ['id', 'delete_time', 'update_time'];

    //关联 banner_item 模型
    public function items()
    {
        return $this->hasMany('BannerItem', 'banner_id', 'id');
    }

    /**
     * 根据ID查找对应的banner信息
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getBannerById($id)
    {
        $banner = self::with(['items', 'items.img'])
            ->find($id);

        return $banner;
    }
}