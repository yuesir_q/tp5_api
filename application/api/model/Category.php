<?php

namespace app\api\model;

use think\Db;

class Category extends BaseModel
{
    protected $hidden = ['delete_time', 'create_time', 'update_time'];

    public function img()
    {
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

    /**
     * 获取所有的分类
     * @return false|static[]
     */
    public static function getAll()
    {
        return self::all([], 'img');
    }
}