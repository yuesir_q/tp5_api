<?php

namespace app\api\model;

use think\Db;

class User extends BaseModel
{
    protected $hidden = ['delete_time', 'update_time', 'create_time'];

    /**
     * 关联地址
     * @return \think\model\relation\HasOne
     */
    public function address()
    {
        return $this->hasOne('UserAddress', 'user_id', 'id');
    }

    /**
     * 通过 openid 获取对应的用户信息
     * @param $openID
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getUserInfoByOpenID($openID)
    {
        return self::where('openid', '=', $openID)->find();
    }
}