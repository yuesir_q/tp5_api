<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v1;


use app\api\model\Theme as ThemeModel;
use app\api\validate\IdCollection;
use app\api\validate\IdMustBePositive;
use app\lib\exception\ThemeMissException;

class Theme
{
    public function getSimpleList($ids = '')
    {
        (new IdCollection())->goCheck();

        $ids    = explode(',', $ids);
//        $result = ThemeModel::with('topicImg,headImg')->select($ids);
        $theme = ThemeModel::with(['topicImg', 'headImg'])->select($ids);
        if ( $theme->isEmpty()) {
            throw new ThemeMissException();
        }
        
        return $theme;
    }

    public function getComplexOne($id =0)
    {
        (new IdMustBePositive())->goCheck();

        $result = ThemeModel::getThemeWithProducts($id);
        if ( !$result) {
            throw new ThemeMissException();
        }

        return $result;
    }
}