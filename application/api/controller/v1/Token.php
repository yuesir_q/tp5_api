<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v1;

use app\api\service\UserToken;
use app\api\validate\TokenGet;

class Token
{
    /**
     * 通过 code 获取 token
     * @param string $code
     * @return array
     * @throws \app\lib\exception\ParameterException
     * @throws \think\Exception
     */
    public function getToken($code = '')
    {
        (new TokenGet())->goCheck();
        
        $ut = new UserToken($code);
        $token = $ut->getToken();

        return [
            'token' => $token
        ];
    }
}