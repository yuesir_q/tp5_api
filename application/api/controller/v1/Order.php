<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\service\UserToken;
use app\api\validate\OrderPlace;
use think\Controller;

class Order extends BaseController
{
    protected $beforeActionList = [
        'checkExclusiveUserScope' => ['only' => 'placeOrder']
    ];

    public function placeOrder()
    {
        (new OrderPlace())->goCheck();

        $uid = UserToken::getUidByToken();
        $orderProducts = input('post.products/a');      //获取所有的订单商品信息
        
        //用户提交商品信息下单
        //查询库存
        //库存足够返回信息给客户端
        //客户端调用微信支付接口
        //查询库存, 够的话服务端调用微信支付接口
        //微信支付返回支付信息 (异步)
        //查询库存
        //支付成功:减除库存, 支付失败不扣除库存
    }
}