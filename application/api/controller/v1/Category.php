<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v1;


use app\api\model\Category as CategoryModel;
use app\lib\exception\ProductMissException;

class Category
{
    //获取最新商品
    public function getAll()
    {
        $categories = CategoryModel::getAll();
        if ( $categories->isEmpty()) throw new ProductMissException();

        return $categories;
    }
}