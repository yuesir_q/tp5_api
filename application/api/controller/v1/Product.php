<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v1;


use app\api\validate\CountValidator;
use app\api\model\Product as ProductModel;
use app\api\validate\IdMustBePositive;
use app\lib\exception\ProductMissException;

class Product
{
    //获取最新商品
    public function getRecent($count = 15)
    {
        (new CountValidator())->goCheck();
        
        $products = ProductModel::getRecent($count);
        if ( $products->isEmpty()) throw new ProductMissException();

        //隐藏 summary 字段的输出
        $products->hidden(['summary']);
        return $products;
    }

    /**
     * 根据分类 ID 获取对应的商品信息
     * @param $categoryId
     * @return false|\PDOStatement|string|\think\Collection
     * @throws ProductMissException
     * @throws \app\lib\exception\ParameterException
     */
    public function getAllByCategoryID($id)
    {
        (new IdMustBePositive())->goCheck();
        
        $products = ProductModel::getAllProductsByCategoryID($id);
        if ( $products->isEmpty()) throw new ProductMissException();

        $products->hidden(['summary']);
        return $products;
    }
    
    public function getOne($id)
    {
        (new IdMustBePositive())->goCheck();
        
        $product = productModel::getProductInfoByID($id);
        if ( empty($product)) {
            throw new ProductMissException();
        }

        return $product;
    }
}