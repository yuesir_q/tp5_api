<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/7/9
 * Time: 下午12:11
 */

namespace app\api\controller;


use app\api\service\Token;
use think\Controller;

class BaseController extends Controller
{
    /**
     * 校验用户权限
     * @throws \app\lib\exception\NoPermissionException
     * @throws \app\lib\exception\TokenMissException
     */
    protected function checkPrimaryScope()
    {
        Token::needUserScope();
    }

    public function checkExclusiveUserScope()
    {
        Token::needExclusiveUserScope();
    }
}