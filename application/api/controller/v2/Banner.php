<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午12:05
 */

namespace app\api\controller\v2;

use app\api\validate\IdMustBePositive;
use app\lib\exception\BannerMissException;
use app\api\model\Banner as BannerModel;

class Banner
{
    public function getBannerById($id)
    {
        (new IdMustBePositive())->goCheck();

//        $banner = \app\api\model\Banner::getBannerById($id);
        $banner = BannerModel::with(['items', 'items.img'])->find($id);
        if ( !$banner) {
            throw new BannerMissException();
        }

        return $banner;
    }
}