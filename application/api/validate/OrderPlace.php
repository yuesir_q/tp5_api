<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午8:02
 */

namespace app\api\validate;


use app\lib\exception\ParameterException;
use think\Exception;

class OrderPlace extends BaseValidate
{
    protected $rule = [
        'products' => 'checkProductParameter',
    ];

    protected $singleRule = [
        'product_id' => 'required|idMustBePositive',
        'count'      => 'required|idMustBePositive'
    ];

    /**
     * 检查下单传递过来的商品信息是否合法
     * @param $products
     * @return bool
     * @throws ParameterException
     */
    protected function checkProductParameter($products)
    {
        if ( !$products) throw new ParameterException('订单商品信息不能为空');
        if ( !is_array($products)) throw new ParameterException('订单商品信息不是数组形式');

        foreach ($products as $val) {
            $this->checkSingleProduct($val);
        }

        return true;
    }

    /**
     * 检查单个商品是否合法
     * @param $value
     */
    protected function checkSingleProduct($value)
    {
        $validator = new BaseValidate($this->singleRule);
        $validator->check($value);
    }
}