<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午1:12
 */

namespace app\api\validate;


class IdCollection extends BaseValidate
{
    protected $rule = [
        'ids'    =>  'require|idCollectionValidate'
    ];

    protected $message = [
        'ids'   => 'ids 必须是以逗号分隔的多个正整数'
    ];

    protected function idCollectionValidate($value)
    {
        $ids = explode(',', $value);
        if ( empty($ids)) return false;
        
        foreach ($ids as $id) {
            if ( !$this->idMustBePositive($id)) {
                return false;
            }
        }

        return true;
    }
}