<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午1:12
 */

namespace app\api\validate;


class TokenGet extends BaseValidate
{
    protected $rule = [
        'code'    =>  'require|isNotEmpty'
    ];

    protected $message = [
        'code'   => '没有 code 还想获取 token, 做梦哦~'
    ];
}