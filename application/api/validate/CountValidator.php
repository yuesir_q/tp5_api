<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午1:12
 */

namespace app\api\validate;


class CountValidator extends BaseValidate
{
    protected $rule = [
        'count'    =>  'idMustBePositive|between:1,15'
    ];

    
}