<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 上午1:12
 */

namespace app\api\validate;


class IdMustBePositive extends BaseValidate
{
    protected $rule = [
        'id'    =>  'require|idMustBePositive'
    ];

    protected $message = [
        'id'    => 'id 必须是正整数'
    ];
}