<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 下午11:28
 */

namespace app\lib\exception;


use think\Exception;

class CategoryMissException extends BaseException
{
    //状态码
    public $code = 404;
    //错误消息
    public $msg = '请求的分类不存在, 请检查参数';
    //自定义错误码
    public $errorCode = 50000;
}