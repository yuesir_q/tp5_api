<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 下午11:28
 */

namespace app\lib\exception;


use think\Exception;

class SuccessMessage
{
    //状态码
    public $code = 201;
    //错误消息
    public $msg = '更新成功';
    //自定义错误码
    public $errorCode = 0;
}