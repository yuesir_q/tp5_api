<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/6/4
 * Time: 下午11:28
 */

namespace app\lib\exception;


use think\Exception;

class NoPermissionException extends BaseException
{
    //状态码
    public $code = 403;
    //错误消息
    public $msg = '没有权限访问对应的接口';
    //自定义错误码
    public $errorCode = 10001;
}