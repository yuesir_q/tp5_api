<?php
/**
 * Created by PhpStorm.
 * User: yuesir
 * Date: 2017/7/9
 * Time: 上午9:25
 */

namespace app\lib\enum;


class UserScope
{
    const USER = 16;

    const SUPERUSER = 32;
}