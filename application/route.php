<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

//Route::rule('hello', 'sample/Test/index', 'GET|POST', ['https' => false]);
Route::get('/api/:version/banner/:id', 'api/:version.Banner/getBannerById');
Route::get('/api/:version/theme', 'api/:version.Theme/getSimpleList');
Route::get('/api/:version/theme/:id', 'api/:version.Theme/getComplexOne');

Route::get('/api/:version/product/recent', 'api/:version.Product/getRecent');
Route::get('/api/:version/product/by_category', 'api/:version.Product/getAllByCategoryID');
Route::get('/api/:version/product/:id', 'api/:version.Product/getOne', [], ['id' => '\d+']);


Route::get('/api/:version/category/all', 'api/:version.Category/getAll');

Route::post('/api/:version/token/user', 'api/:version.Token/getToken');

//新增地址
Route::post('/api/:version/address', 'api/:version.Address/createOrUpdate');
//Route::post('hello', 'sample/Test/index');
//Route::any('hello', 'sample/Test/index');
//return [
//    '__pattern__' => [
//        'name' => '\w+',
//    ],
//    '[hello]'     => [
//        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
//        ':name' => ['index/hello', ['method' => 'post']],
//    ],
//
//];
